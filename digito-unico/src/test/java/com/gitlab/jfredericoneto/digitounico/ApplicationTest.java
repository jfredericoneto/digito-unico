package com.gitlab.jfredericoneto.digitounico;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.OK;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ApplicationTest {
    

    @Test
    void deveEncontrarAOpenAPI() {
        given().header(ACCEPT, APPLICATION_JSON).when().get("/openapi").then()
                .statusCode(OK.getStatusCode());
    }

    @Test
    void deveEncontrarASwaggerUI() {
        given().when().get("/swagger-ui").then().statusCode(OK.getStatusCode());
    }
}