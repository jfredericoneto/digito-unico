package com.gitlab.jfredericoneto.digitounico.resource;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Random;

import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoDTO;
import com.gitlab.jfredericoneto.digitounico.dto.UsuarioDTO;

import org.hamcrest.core.Is;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.common.mapper.TypeRef;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UsuarioReourceTest {

    private static final String NOME_PADRAO = "João José da Silva Xavier";
    private static final String OUTRO_NOME = "Tiradentes";
    private static final String EMAIL_PADRAO = "joao.jsx@gmail.com";
    private static final String OUTRO_EMAIL = "tiradentes@gmail.com";

    private static final int USUARIOS_SIZE = 0;
    private static String usuarioId;
    private static String chavePublica;

    @Test
    void deveRetornaNaoEncontradoParaUsuarioInesistente() {
        Long randomId = new Random().nextLong();
        given().pathParam("id", randomId).when().get("/api/usuario/{id}").then().statusCode(NOT_FOUND.getStatusCode());
    }

    @Test
    @Order(1)
    void deveObterQuantidadeItensIniciais() {
        List<UsuarioDTO> usuarios = get("/api/usuario").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).extract().body().as(getUsuarioDTOTypeRef());
        assertEquals(USUARIOS_SIZE, usuarios.size());
    }

    @Test
    @Order(2)
    void deveAdicionarUmUsuario() {
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.nome = NOME_PADRAO;
        usuario.email = EMAIL_PADRAO;

        String localizacaoRecurso = given().body(usuario).header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON).when().post("/api/usuario").then().statusCode(CREATED.getStatusCode())
                .extract().header("Location");
        assertTrue(localizacaoRecurso.contains("/api/usuario"));

        // Stores the id
        String[] segmentos = localizacaoRecurso.split("/");
        usuarioId = segmentos[segmentos.length - 1];
        assertNotNull(usuarioId);

        given().pathParam("id", usuarioId).when().get("/api/usuario/{id}").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).body("nome", Is.is(NOME_PADRAO))
                .body("email", Is.is(EMAIL_PADRAO));

        List<UsuarioDTO> usuarios = get("/api/usuario").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).extract().body().as(getUsuarioDTOTypeRef());
        assertEquals(USUARIOS_SIZE + 1, usuarios.size());
    }

    @Test
    @Order(3)
    void deveObterUmUsuarioPeloId() {

        given().pathParam("id", usuarioId).header(CONTENT_TYPE, APPLICATION_JSON).header(ACCEPT, APPLICATION_JSON)
                .when().get("/api/usuario/{id}").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).body("nome", Is.is(NOME_PADRAO))
                .body("email", Is.is(EMAIL_PADRAO));
    }

    @Test
    @Order(4)
    void deveAlterarUsuario() {
        UsuarioDTO usuario = new UsuarioDTO();
        usuario.id = Long.valueOf(usuarioId);
        usuario.nome = OUTRO_NOME;
        usuario.email = OUTRO_EMAIL;

        given().pathParam("id", usuarioId).body(usuario).header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON).when().put("/api/usuario/{id}").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).body("nome", Is.is(OUTRO_NOME))
                .body("email", Is.is(OUTRO_EMAIL));

        List<UsuarioDTO> usuarios = get("/api/usuario").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).extract().body().as(getUsuarioDTOTypeRef());
        assertEquals(USUARIOS_SIZE + 1, usuarios.size());
    }

    @Test
    @Order(5)
    void deveObterListaDeDigitosDoUsuarioPeloId() {
        given().pathParam("id", usuarioId).header(CONTENT_TYPE, APPLICATION_JSON).header(ACCEPT, APPLICATION_JSON)
                .when().get("/api/usuario/{id}/digitounico").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).extract().body().as(getDigitoUnicoDTOTypeRef());
    }

    @Test
    @Order(6)
    void deveGerarChavesDevolverChavePublica() {

        String chave = given().pathParam("id", usuarioId).header(CONTENT_TYPE, APPLICATION_JSON).when()
                .post("/api/usuario/{id}/chaves").then().statusCode(OK.getStatusCode()).extract().asString();

        chavePublica = chave;
        Assertions.assertNotNull(chave);
        Assertions.assertTrue(chave.getClass() == String.class);
    }

    @Test
    @Order(7)
    void deveEncriptarOsDadosDoUsuario() {
        given().pathParam("id", usuarioId).body(chavePublica).header(CONTENT_TYPE, TEXT_PLAIN)
                .header(ACCEPT, APPLICATION_JSON).when().put("/api/usuario/{id}/encriptar").then()
                .statusCode(OK.getStatusCode()).header(CONTENT_TYPE, APPLICATION_JSON);
    }

    @Test
    @Order(8)
    void deveDecriptarOsDadosDoUsuario() {

        given().pathParam("id", usuarioId).body(chavePublica).header(CONTENT_TYPE, TEXT_PLAIN)
                .header(ACCEPT, APPLICATION_JSON).when().put("/api/usuario/{id}/decriptar").then()
                .statusCode(OK.getStatusCode()).header(CONTENT_TYPE, APPLICATION_JSON).body("nome", Is.is(OUTRO_NOME))
                .body("email", Is.is(OUTRO_EMAIL));
    }

    @Test
    @Order(9)
    void deveRemoverUmUsuario() {
        given().pathParam("id", usuarioId).when().delete("/api/usuario/{id}").then()
                .statusCode(NO_CONTENT.getStatusCode());

        List<UsuarioDTO> usuarios = get("/api/usuario").then().statusCode(OK.getStatusCode())
                .header(CONTENT_TYPE, APPLICATION_JSON).extract().body().as(getUsuarioDTOTypeRef());
        assertEquals(USUARIOS_SIZE, usuarios.size());
    }

    private TypeRef<List<UsuarioDTO>> getUsuarioDTOTypeRef() {
        return new TypeRef<List<UsuarioDTO>>() {

        };
    }

    private TypeRef<List<DigitoUnicoDTO>> getDigitoUnicoDTOTypeRef() {
        return new TypeRef<List<DigitoUnicoDTO>>() {

        };
    }
}