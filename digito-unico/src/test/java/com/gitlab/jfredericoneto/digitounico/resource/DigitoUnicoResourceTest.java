package com.gitlab.jfredericoneto.digitounico.resource;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoParametroDTO;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class DigitoUnicoResourceTest {

    private static final String DIGITO_UM = "116";
    private static final String DIGITO_DOIS = "9875";
    private static final Integer QUANT_CONCATENACAO_UM = 1;
    private static final Integer QUANT_CONCATENACAO_DOIS = 4;

    private static final Integer RESULTADO_UM = 8;
    private static final Integer RESULTADO_DOIS = 32;
    private static final Integer RESULTADO_TRES = 29;
    private static final Integer RESULTADO_QUATRO = 116;

    @Test
    void deveCalcularCorretamenteDigitoUnicoResultadoUm() {
        DigitoUnicoParametroDTO digitoUnicoParametro = new DigitoUnicoParametroDTO();
        digitoUnicoParametro.digito = DIGITO_UM;
        digitoUnicoParametro.quantConcatenacao = QUANT_CONCATENACAO_UM;

        String retorno = given().body(digitoUnicoParametro).header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON).when().post("/api/digitounico/calcular").then()
                .statusCode(OK.getStatusCode()).extract().asString();

        assertEquals(RESULTADO_UM, Integer.parseInt(retorno));
    }

    @Test
    void deveCalcularCorretamenteDigitoUnicoResultadoDois() {
        DigitoUnicoParametroDTO digitoUnicoParametro = new DigitoUnicoParametroDTO();
        digitoUnicoParametro.digito = DIGITO_UM;
        digitoUnicoParametro.quantConcatenacao = QUANT_CONCATENACAO_DOIS;

        String retorno = given().body(digitoUnicoParametro).header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON).when().post("/api/digitounico/calcular").then()
                .statusCode(OK.getStatusCode()).extract().asString();

        assertEquals(RESULTADO_DOIS, Integer.parseInt(retorno));
    }

    @Test
    void deveCalcularCorretamenteDigitoUnicoResultadoTres() {
        DigitoUnicoParametroDTO digitoUnicoParametro = new DigitoUnicoParametroDTO();
        digitoUnicoParametro.digito = DIGITO_DOIS;
        digitoUnicoParametro.quantConcatenacao = QUANT_CONCATENACAO_UM;

        String retorno = given().body(digitoUnicoParametro).header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON).when().post("/api/digitounico/calcular").then()
                .statusCode(OK.getStatusCode()).extract().asString();

        assertEquals(RESULTADO_TRES, Integer.parseInt(retorno));
    }

    @Test
    void deveCalcularCorretamenteDigitoUnicoResultadoQuatro() {
        DigitoUnicoParametroDTO digitoUnicoParametro = new DigitoUnicoParametroDTO();
        digitoUnicoParametro.digito = DIGITO_DOIS;
        digitoUnicoParametro.quantConcatenacao = QUANT_CONCATENACAO_DOIS;

        String retorno = given().body(digitoUnicoParametro).header(CONTENT_TYPE, APPLICATION_JSON)
                .header(ACCEPT, APPLICATION_JSON).when().post("/api/digitounico/calcular").then()
                .statusCode(OK.getStatusCode()).extract().asString();

        assertEquals(RESULTADO_QUATRO, Integer.parseInt(retorno));
    }
}