package com.gitlab.jfredericoneto.digitounico.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import javax.ws.rs.NotFoundException;

import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoDTO;
import com.gitlab.jfredericoneto.digitounico.dto.UsuarioDTO;
import com.gitlab.jfredericoneto.digitounico.entity.Usuario;
import com.gitlab.jfredericoneto.digitounico.mapper.UsuarioMapper;

@ApplicationScoped
@Transactional(TxType.REQUIRED)
public class UsuarioService {

    @Inject
    UsuarioMapper usuarioMapper;

    @Inject
    DigitoUnicoService digitoUnicoService;

    @Inject
    CriptografiaService criptografiaService;

    @Transactional(TxType.SUPPORTS)
    public UsuarioDTO get(Long id) {
        Optional<Usuario> usuarioOp = Usuario.findByIdOptional(id);

        if (usuarioOp.isEmpty()) {
            throw new NotFoundException();
        }

        return usuarioMapper.toUsuarioDTO(usuarioOp.get());
    }

    @Transactional(TxType.SUPPORTS)
    public List<UsuarioDTO> list() {
        List<Usuario> usuarios = Usuario.listAll();
        return usuarios.stream().map(usuarioMapper::toUsuarioDTO).collect(Collectors.toList());
    }

    public UsuarioDTO add(UsuarioDTO dto) {
        Usuario entity = usuarioMapper.toUsuario(dto);
        Usuario.persist(entity);
        return usuarioMapper.toUsuarioDTO(entity);
    }

    public UsuarioDTO update(Long id, UsuarioDTO dto) {
        Optional<Usuario> usuarioOp = Usuario.findByIdOptional(id);

        if (usuarioOp.isEmpty()) {
            throw new NotFoundException();
        }

        Usuario entity = usuarioMapper.toUsuario(dto);

        Usuario usuario = usuarioOp.get();
        usuario.nome = entity.nome;
        usuario.email = entity.email;
        Usuario.persist(usuario);
        return usuarioMapper.toUsuarioDTO(usuario);
    }

    public void delete(Long id) {
        Optional<Usuario> usuarioOp = Usuario.findByIdOptional(id);

        usuarioOp.ifPresentOrElse(Usuario::delete, () -> {
            throw new NotFoundException();
        });
    }

    @Transactional(TxType.SUPPORTS)
    public List<DigitoUnicoDTO> listAllDigitoUnico(Long id) {

        Optional<Usuario> usuarioOp = Usuario.findByIdOptional(id);

        if (usuarioOp.isEmpty()) {
            throw new NotFoundException();
        }

        List<DigitoUnicoDTO> lista = digitoUnicoService.listByUsuario(id);
        return lista;
    }

    @Transactional(TxType.SUPPORTS)
    public String criarChaves(Long id) {
        Optional<Usuario> usuarioOp = Usuario.findByIdOptional(id);

        if (usuarioOp.isEmpty()) {
            throw new NotFoundException();
        }

        return criptografiaService.criarChaves(id);
    }

    public UsuarioDTO encriptar(Long id, String chavePublica) {

        Optional<Usuario> usuarioOp = Usuario.findByIdOptional(id);

        if (usuarioOp.isEmpty()) {
            throw new NotFoundException();
        }

        Usuario usuario = usuarioOp.get();
        usuario.nome = criptografiaService.encriptar(chavePublica, usuario.nome);
        usuario.email = criptografiaService.encriptar(chavePublica, usuario.email);
        Usuario.persist(usuario);
        return usuarioMapper.toUsuarioDTO(usuario);
    }

    public UsuarioDTO decriptar(Long id, String chavePublica) {

        if (chavePublica == null) {
            throw new NotFoundException("Chave Pública obrigatória");
        }

        Optional<Usuario> usuarioOp = Usuario.findByIdOptional(id);

        if (usuarioOp.isEmpty()) {
            throw new NotFoundException();
        }

        Usuario usuario = usuarioOp.get();
        usuario.nome = criptografiaService.decriptar(id, chavePublica, usuario.nome);
        usuario.email = criptografiaService.decriptar(id, chavePublica, usuario.email);
        Usuario.persist(usuario);
        return usuarioMapper.toUsuarioDTO(usuario);
    }

}