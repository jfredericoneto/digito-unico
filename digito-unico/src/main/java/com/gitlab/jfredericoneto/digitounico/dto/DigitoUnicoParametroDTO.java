package com.gitlab.jfredericoneto.digitounico.dto;

public class DigitoUnicoParametroDTO {

    public Long usuarioId;
	public String digito;
	public Integer quantConcatenacao;

}