package com.gitlab.jfredericoneto.digitounico;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.servers.Server;

@ApplicationPath("/")
@OpenAPIDefinition(info = @Info(title = "Dígito Único API", description = "API para calculo de dígito único", version = "1.0", contact = @Contact(name = "José Frederico Neto", url = "https://gitlab.com/jfredericoneto", email = "jfredericon@gmail.com")), servers = {
        @Server(url = "http://localhost:8080") })
public class DigitoUnicoApplication extends Application {

}