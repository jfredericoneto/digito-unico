package com.gitlab.jfredericoneto.digitounico.service;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.enterprise.context.ApplicationScoped;

import org.hibernate.service.spi.ServiceException;

@ApplicationScoped
public class CriptografiaService {

    private static final Map<Long, KeyPair> chavesGeradas = new HashMap<>();
    private static final Base64.Decoder DECODER = Base64.getDecoder();
    private static final Base64.Encoder ENCODER = Base64.getEncoder();

    public String criarChaves(Long usuarioId) {

        try {

            KeyPairGenerator geradorChave = KeyPairGenerator.getInstance("RSA");
            geradorChave.initialize(2048);
            KeyPair chavesPar = geradorChave.generateKeyPair();
            chavesGeradas.put(usuarioId, chavesPar);

            return ENCODER.encodeToString(chavesPar.getPublic().getEncoded());
        } catch (Exception e) {
            throw new ServiceException("Erro ao gerar par de chaves.");
        }

    }

    public String encriptar(String chavePublica, String valor) {
        try {
            PublicKey chavePublicaConvertida = converterChavePublica(chavePublica);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, chavePublicaConvertida);
            byte[] valorBytes = cipher.doFinal(valor.getBytes());
            return ENCODER.encodeToString(valorBytes);
        } catch (Exception e) {
            throw new ServiceException("Erro ao encriptar.");
        }
    }

    public String decriptar(Long usuarioId, String chavePublica, String valor) {
        try {
            PublicKey chavePublicaConvertida = converterChavePublica(chavePublica);

            if (!chavePublicaConvertida.equals(chavesGeradas.get(usuarioId).getPublic())) {
                throw new ServiceException("A chave publica não é válida.");
            }

            PrivateKey cahvePrivada = chavesGeradas.get(usuarioId).getPrivate();
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, cahvePrivada);
            byte[] valorBytes = DECODER.decode(valor);
            return new String(cipher.doFinal(valorBytes), "UTF8");
        } catch (Exception e) {
            throw new ServiceException("Erro ao decriptar.");
        }
    }

    private static PublicKey converterChavePublica(String chavePublica) {
        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(chavePublica.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        } catch (Exception e) {
            throw new ServiceException("Chave pública inválida.");
        }
    }
}