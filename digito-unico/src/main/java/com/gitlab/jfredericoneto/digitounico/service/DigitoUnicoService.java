package com.gitlab.jfredericoneto.digitounico.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoDTO;
import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoParametroDTO;
import com.gitlab.jfredericoneto.digitounico.entity.DigitoUnico;
import com.gitlab.jfredericoneto.digitounico.entity.Usuario;
import com.gitlab.jfredericoneto.digitounico.mapper.DigitoUnicoMapper;

import io.quarkus.panache.common.Parameters;

@ApplicationScoped
public class DigitoUnicoService {

    @Inject
    DigitoUnicoMapper digitoUnicoMapper;

    @Inject
    CacheService cacheService;

    @Inject
    UsuarioService usuarioService;

    @Transactional(TxType.SUPPORTS)
    public List<DigitoUnicoDTO> listByUsuario(Long id) {

        List<DigitoUnico> lista = DigitoUnico.list("usuario_id", id);
        return lista.stream().map(digitoUnicoMapper::toDigitoUnicoDTO).collect(Collectors.toList());
    }

    @Transactional(TxType.REQUIRED)
    public Integer calcular(DigitoUnicoParametroDTO dto) {

        Usuario usuario = null;

        if (dto.usuarioId != null) {
            usuarioService.get(dto.usuarioId);
            usuario = Usuario.findById(dto.usuarioId);
        }

        if(dto.quantConcatenacao == null){
            dto.quantConcatenacao = 1;
        }

        Integer digitoUnico = cacheService.buscar(dto.digito, dto.quantConcatenacao);

        if (digitoUnico == null) {
            digitoUnico = definirDigitoUnico(dto.digito, dto.quantConcatenacao);

            cacheService.adicionar(dto.digito, dto.quantConcatenacao, digitoUnico);
        }

        DigitoUnico digitoUnicoEntity = new DigitoUnico();
        digitoUnicoEntity.digitoParametro = dto.digito;
        digitoUnicoEntity.quantConcatenacao = dto.quantConcatenacao;
        digitoUnicoEntity.digitoUnico = digitoUnico;

        if (usuario != null) {
            DigitoUnico exitente = DigitoUnico
                    .find("usuario_id = :usuario and digitoParametro = :digito and quantConcatenacao = :concatenacao",
                            Parameters.with("usuario", dto.usuarioId).and("digito", dto.digito)
                                    .and("concatenacao", dto.quantConcatenacao).map())
                    .firstResult();

            if (exitente == null) {
                digitoUnicoEntity.usuario = usuario;
            }
        }

        digitoUnicoEntity.persist();

        return digitoUnico;
    }

    private static Integer definirDigitoUnico(String digitoParametro, Integer quantConcatenacao) {
        if (digitoParametro.isEmpty() || digitoParametro.trim().equals("")) {
            return null;
        }

        if (quantConcatenacao != null) {
            String novoDigito = concatenar(digitoParametro, quantConcatenacao);
            return digitoUnico(new BigInteger(novoDigito));
        } else {
            return digitoUnico(new BigInteger(digitoParametro));
        }
    }

    private static String concatenar(String digito, Integer concatenacao) {
        String digitoConcatenado = String.valueOf("");
        for (int i = 0; i < concatenacao; i++) {
            digitoConcatenado = digitoConcatenado.concat(digito);
        }
        return digitoConcatenado;
    }

    private static Integer digitoUnico(BigInteger digito) {

        List<BigInteger> digitos = converterEmDigitos(digito);

        BigInteger resultado = digitos.stream().reduce(BigInteger.ZERO, (subtotal, valor) -> subtotal.add(valor));

        if (BigInteger.TEN.pow(1000000).compareTo(resultado) < 0) {
            return digitoUnico(resultado);
        }

        return resultado.intValue();
    }

    private static List<BigInteger> converterEmDigitos(BigInteger digito) {
        List<BigInteger> digitos = new ArrayList<>();
        char[] str = digito.toString().toCharArray();
        for (char digitoChar : str) {
            String digitoStr = String.valueOf(digitoChar);
            digitos.add(new BigInteger(digitoStr));
        }

        return digitos;
    }

}