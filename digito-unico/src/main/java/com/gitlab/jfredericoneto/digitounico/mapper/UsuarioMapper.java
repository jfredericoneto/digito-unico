package com.gitlab.jfredericoneto.digitounico.mapper;

import com.gitlab.jfredericoneto.digitounico.dto.UsuarioDTO;
import com.gitlab.jfredericoneto.digitounico.entity.Usuario;

import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface UsuarioMapper {
    
    public Usuario toUsuario(UsuarioDTO dto);
    public UsuarioDTO toUsuarioDTO(Usuario usuario);

}