package com.gitlab.jfredericoneto.digitounico.dto;

public class DigitoUnicoDTO {
    
    public Long id;
	public String digitoParametro;
	public Integer quantConcatenacao;
	public Integer digitoUnico;
}