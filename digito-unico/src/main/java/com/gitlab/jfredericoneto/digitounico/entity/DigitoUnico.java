package com.gitlab.jfredericoneto.digitounico.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class DigitoUnico extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "digito_unico_id")
    public Long id;

    @NotNull
    @Column(name = "digito_parametro")
    public String digitoParametro;

    @NotNull
    @Column(name = "quant_concatenacao")
    public Integer quantConcatenacao;

    @NotNull
    @Column(name = "digito_unico")
    public Integer digitoUnico;

    @JoinColumn(name = "usuario_id")
    @ManyToOne(fetch = FetchType.LAZY)
    public Usuario usuario;

    public DigitoUnico() {
        
    }

}