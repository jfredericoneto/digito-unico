package com.gitlab.jfredericoneto.digitounico.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name = "usuarios")
public class Usuario extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuario_id")
    public Long id;

    @NotNull
    @Column(name = "nome", length = 5000)
    public String nome;

    @NotNull
    @Column(name = "email", length = 5000)
    public String email;

    @OneToMany(mappedBy = "usuario", targetEntity = DigitoUnico.class, fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    public Set<DigitoUnico> digitosUnicos = new HashSet<>();

    public Usuario() {

    }

}