package com.gitlab.jfredericoneto.digitounico.service;

import java.util.LinkedHashMap;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CacheService {

    private static LinkedHashMap<String, Integer> listaCache = new LinkedHashMap<>();

    public void adicionar(String digitoParametro, Integer quantConcatenacao, Integer digitoUnico) {
        if (listaCache.size() == 10) {
            String primeiraChave = listaCache.keySet().stream().findFirst().get();
            listaCache.remove(primeiraChave);
        }

        String chave = criarChave(digitoParametro, quantConcatenacao);
        if (!listaCache.containsKey(chave)) {
            listaCache.put(chave, digitoUnico);
        }
    }

    public Integer buscar(String digitoParametro, Integer quantConcatenacao) {
        Integer digitoUnico = null;
        String chave = criarChave(digitoParametro, quantConcatenacao);
        if (listaCache.containsKey(chave)) {
            digitoUnico = listaCache.get(chave);
        }
        return digitoUnico;
    }

    public void limpar() {
        listaCache.clear();
    }

    private static String criarChave(String digitoParametro, Integer quantConcatenacao) {
        String chave = digitoParametro.concat("_").concat(String.valueOf(quantConcatenacao));

        return chave;
    }

}