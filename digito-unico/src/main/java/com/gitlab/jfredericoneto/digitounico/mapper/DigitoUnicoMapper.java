package com.gitlab.jfredericoneto.digitounico.mapper;

import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoDTO;
import com.gitlab.jfredericoneto.digitounico.entity.DigitoUnico;

import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface DigitoUnicoMapper {
    
    public DigitoUnico toDigitoUnico(DigitoUnicoDTO dto);
    public DigitoUnicoDTO toDigitoUnicoDTO(DigitoUnico digitoUnico);

}