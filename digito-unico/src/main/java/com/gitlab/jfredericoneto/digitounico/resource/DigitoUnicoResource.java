package com.gitlab.jfredericoneto.digitounico.resource;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoParametroDTO;
import com.gitlab.jfredericoneto.digitounico.service.DigitoUnicoService;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@Path("/api/digitounico")
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Dígito Único")
public class DigitoUnicoResource {

    @Inject
    DigitoUnicoService digitoUnicoService;

    @Operation(summary = "Realiza o calculo do dígito único.")
    @APIResponse(responseCode = "200", description = "Calculo realizado com sucesso")
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @POST
    @Path("calcular")
    public Response calc(
            @RequestBody(required = true, content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = DigitoUnicoParametroDTO.class))) DigitoUnicoParametroDTO dto) {

        Integer digitoUnico = digitoUnicoService.calcular(dto);

        return Response.ok(digitoUnico).build();
    }

}