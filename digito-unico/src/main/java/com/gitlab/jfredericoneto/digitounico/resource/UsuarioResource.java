package com.gitlab.jfredericoneto.digitounico.resource;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.gitlab.jfredericoneto.digitounico.dto.DigitoUnicoDTO;
import com.gitlab.jfredericoneto.digitounico.dto.UsuarioDTO;
import com.gitlab.jfredericoneto.digitounico.service.UsuarioService;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@Path("/api/usuario")
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Usuário")
public class UsuarioResource {

    @Inject
    UsuarioService usuarioService;

    @Operation(summary = "Retorna um usuário pelo id.")
    @APIResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = UsuarioDTO.class)))
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @GET
    @Path("{id}")
    public Response get(@PathParam("id") Long id) {
        UsuarioDTO usuario = usuarioService.get(id);
        return Response.ok(usuario).build();
    }

    @Operation(summary = "Retorna todos os usuaŕios.")
    @APIResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = UsuarioDTO.class, type = SchemaType.ARRAY)))
    @GET
    public Response list() {
        List<UsuarioDTO> usuarios = usuarioService.list();

        return Response.ok(usuarios).build();
    }

    @Operation(summary = "Cria um usuário.")
    @APIResponse(responseCode = "201", description = "A URI do usuario criado.", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = URI.class)))
    @POST
    public Response add(
            @RequestBody(required = true, content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = UsuarioDTO.class))) @Valid UsuarioDTO dto,
            @Context UriInfo uriInfo) {

        UsuarioDTO usuario = usuarioService.add(dto);
        UriBuilder builder = uriInfo.getAbsolutePathBuilder().path(Long.toString(usuario.id));
        return Response.created(builder.build()).build();
    }

    @Operation(summary = "Altera os dados de um usuário existente.")
    @APIResponse(responseCode = "200", description = "Usuário alterado com sucesso.", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = UsuarioDTO.class)))
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @PUT
    @Path("{id}")
    public Response update(
            @RequestBody(required = true, content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = UsuarioDTO.class))) @PathParam("id") Long id,
            @Valid UsuarioDTO dto) {

        UsuarioDTO usuario = usuarioService.update(id, dto);
        return Response.ok(usuario).build();
    }

    @Operation(summary = "Deleta um usuário existente.")
    @APIResponse(responseCode = "204", description = "Usuário deletado com sucesso.")
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id) {
        usuarioService.delete(id);
    }

    @Operation(summary = "Retorna todos os digitos calculados pora um usuário ativo.")
    @APIResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = DigitoUnicoDTO.class, type = SchemaType.ARRAY)))
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @GET
    @Path("{id}/digitounico")
    public Response listByUsuario(@PathParam("id") Long id) {
        List<DigitoUnicoDTO> digitos = usuarioService.listAllDigitoUnico(id);

        return Response.ok(digitos).build();
    }

    @Operation(summary = "Gera as chaves de criptografia do usuário e retorna a chave publica")
    @APIResponse(responseCode = "200", description = "Chaves criadas com sucesso.")
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{id}/chaves")
    public Response criarChaves(@PathParam("id") Long id) {

        String chavePublica = usuarioService.criarChaves(id);

        return Response.ok(chavePublica).build();
    }

    @Operation(summary = "Encripta os dados de um usuário existente.")
    @APIResponse(responseCode = "200", description = "Dados encriptado com sucesso.", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = UsuarioDTO.class)))
    @APIResponse(responseCode = "400", description = "Chave Pública obrigatória.")
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @PUT
    @Path("{id}/encriptar")
    public Response encriptar(
            @RequestBody(required = true, content = @Content(mediaType = MediaType.TEXT_PLAIN)) @PathParam("id") Long id,
            String chavePublica) {

        if (chavePublica == null || chavePublica.trim().equals("")) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        UsuarioDTO usuario = usuarioService.encriptar(id, chavePublica);
        return Response.ok(usuario).build();
    }

    @Operation(summary = "Decripta os dados de um usuário existente.")
    @APIResponse(responseCode = "200", description = "Dados decriptados com sucesso.", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = UsuarioDTO.class)))
    @APIResponse(responseCode = "400", description = "Chave Pública obrigatória.")
    @APIResponse(responseCode = "404", description = "Usuário não encontrado.")
    @PUT
    @Path("{id}/decriptar")
    public Response decriptar(
            @RequestBody(required = true, content = @Content(mediaType = MediaType.TEXT_PLAIN)) @PathParam("id") Long id,
            String chavePublica) {

        if (chavePublica == null || chavePublica.trim().equals("")) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        UsuarioDTO usuario = usuarioService.decriptar(id, chavePublica);
        return Response.ok(usuario).build();
    }
}