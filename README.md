# Desafio Dígito Único

# Tecnologias utilizadas

* Java 11 - https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
* Maven - https://maven.apache.org
* Quarkus - https://quarkus.io
* H2 Database Engine - https://www.h2database.com/html/main.html
* Hibernate ORM com Panache - https://quarkus.io/guides/hibernate-orm-panache
* Mapstruct - https://mapstruct.org
* Swagger - https://swagger.io/specification

# Como executar esse projeto localmente

## Requisitos: 

* Git - https://git-scm.com/downloads
* Java 11 - https://www.oracle.com/java/technologies/javase-jdk11-downloads.html

## Passos a passo: 

1 - Clonar o repositório

``` bash
git clone https://gitlab.com/jfredericoneto/digito-unico.git desafio
```

2 - Entrar na pasta do projeto Quarkus

``` bash
cd desafio/digito-unico
```
  
3 - Rodar a aplicação

**Usando modo de desenvolvimento do Quarkus**

``` bash
./mvnw quarkus:dev
```

**Gerando o .jar e executando com o Java**

**OBS:** Caso opte por essa opção os testes serão executados na geração do pacote.

``` bash
./mvnw package
```

``` bash
java -jar target/digito-unico-1.0.0-SNAPSHOT-runner.jar 
```

- A aplicação estará disponivel em http://localhost:8080/api
- Os recursos disponíveis são /usuarios e /digitoUnico
- Documentação em http://localhost:8080/swagger-ui

## Como rodar os testes

``` bash
./mvnw test
```

## Testes com o Postman
**ATENÇÃO:** Certifique-se de importar os arquivos para realizar os testes corretamente.

1 - Pimeiro você  deve importar a collection que está na raiz do repositório (**postman_collection.json**)

2 - Depois vocẽ deve importar o arquivo de environment que está na raiz do repositório (**postman_enviroment.json**)

3 - Você deve clicar em Runner selecionar a collection **Dígito Único** o environment  **DigitoUnicoEnv** e clicar em **Run Dígito Uníco**, então todos os testes serão realizados

## SwaggerUI

Disponível em http://localhost:8080/swagger-ui